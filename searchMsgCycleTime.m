function [ cycleTimeOfSearchedMessage ] = searchMsgCycleTime(dbcpath,searchID )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

fileID = fopen(dbcpath, 'r');
fileContent = fscanf(fileID, '%c');
fclose(fileID);

% Extrahiere die Nachrichts-ID und Zykluszeit aus der Zeile
tokens = regexp(fileContent, 'BA_ "GenMsgCycleTime" BO_ (?<ID>\d+)\s(?<CycleTime>\d+);', 'names');
    
%%
    for i = 1:length(tokens);
        tokens(i).ID = str2double(tokens(i).ID);
        tokens(i).CycleTime = str2double(tokens(i).CycleTime);
    end
%%  
for i = 1:length(tokens)
    if isequal(tokens(i).ID,searchID)
        matchID = i;
    end
end
cycleTimeOfSearchedMessage = tokens(matchID).CycleTime;
end

