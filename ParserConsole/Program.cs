﻿using ParserLibrary;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace ParserConsole // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            DbcParser parser = new DbcParser();
            string path = "C:\\Git\\common-files\\CAN Database\\CAN1.dbc";
            List<Message> messagelist = parser.ParseDBC(path);
            int count = messagelist.Count();
            int identifier = messagelist[0].Id;

            Console.WriteLine(identifier.ToString());

            var json = JsonSerializer.Serialize(messagelist);
        }
    }
}