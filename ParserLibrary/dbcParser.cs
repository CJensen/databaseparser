﻿using System.Text.RegularExpressions;

namespace ParserLibrary
{
    public class Message
    {
        private int id;
        private int len;
        private string name;
        private string emitter;
        private int period;
        private string type;
        private List<Signal> signal = new List<Signal>();

        public int Id { get => id; set => id = value; }
        public int Len { get => len; set => len = value; }
        public string Name { get => name; set => name = value; }
        public string Emitter { get => emitter; set => emitter = value; }
        public int Period { get => period; set => period = value; }
        public string Type { get => type; set => type = value; }
        internal List<Signal> Signal { get => signal; set => signal = value; }
    }

    public class Signal
    {
        private string? name;
        private int msb;
        private int len;
        private float fac;
        private float offset;
        private string? unit;
        private float max;
        private float min;
        private char receiver;
        private char values;
        private bool signed;
        private bool formatMotorola;

        public string Name { get => name; set => name = value; }
        public int Msb { get => msb; set => msb = value; }
        public int Len { get => len; set => len = value; }
        public float Fac { get => fac; set => fac = value; }
        public float Offset { get => offset; set => offset = value; }
        public string Unit { get => unit; set => unit = value; }
        public float Max { get => max; set => max = value; }
        public float Min { get => min; set => min = value; }
        public char Receiver { get => receiver; set => receiver = value; }
        public char Values { get => values; set => values = value; }
        public bool Signed { get => signed; set => signed = value; }
        public bool FormatMotorola { get => formatMotorola; set => formatMotorola = value; }
    }
    public class DbcParser
    {

        // Methods
        public string ReplaceFormat(Match m)
        {
            string replaced = "";
            if (m.Value == "@0-")
                replaced = " 0 0";
            else if (m.Value == "@0+")
                replaced = " 0 1";
            else if (m.Value == "@1-")
                replaced = " 1 0";
            else if (m.Value == "@1+")
                replaced = " 1 1";
            return replaced;
        }

        public List<Message> ParseDBC(string pathofdbc)
        {
            string text = System.IO.File.ReadAllText(pathofdbc);

            List<Message> foundMessages = new List<Message>();
            MatchEvaluator myEvaluator = new MatchEvaluator(ReplaceFormat);
            Regex rx = new Regex(@"\s+[0-9]{1,4}\s+[0-9a-zA-Z_]+\s+[0-9]+\s+[0-9a-zA-Z_]+\r\n(.+\r\n)+");

            text = Regex.Replace(text, @"(BO_)|(SG_)|\(|\)|\[|\]|:", "");
            text = Regex.Replace(text, @"@[0,1][\+,\-]", myEvaluator);
            string cleanertext = Regex.Replace(text, @",|\|", " ");

            MatchCollection matches = rx.Matches(cleanertext);

            foreach (Match match in matches)
            {
                Message msg = new Message();
                string[] lines = match.Value.Trim().Split("\r\n");
                string[] msgline = lines[0].Split(" ");

                msg.Id = Convert.ToInt32(msgline[0]);
                msg.Name = msgline[1];
                msg.Len = Convert.ToInt32(msgline[2]);
                msg.Emitter = msgline[3];


                string[] loines = lines.Skip(1).ToArray();

                foreach (string signalLine in lines.Skip(1).ToArray())
                {
                    string[] sig = signalLine.Trim().Split(' ', StringSplitOptions.RemoveEmptyEntries);

                    Signal sgnl = new Signal();
                    msg.Signal.Add(sgnl);
                    sgnl.Name = sig[0];
                    sgnl.Msb = Convert.ToInt32(sig[1]);
                    sgnl.Len = Convert.ToInt32(sig[2]);
                    sgnl.FormatMotorola = sig[3] == "0";
                    sgnl.Signed = sig[4] == "0";
                    sgnl.Fac = float.Parse(sig[5]);
                    sgnl.Offset = float.Parse(sig[6]);
                    sgnl.Min = float.Parse(sig[7]);
                    sgnl.Max = float.Parse(sig[8]);
                    sgnl.Unit = sig[9];
                }
                foundMessages.Add(msg);
            }
            return foundMessages;
        }
    }
}